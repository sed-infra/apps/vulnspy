import logging
import os
import sys
import time
import xml.etree.ElementTree as ET
from datetime import date, datetime, timedelta, timezone
from enum import Enum

import requests
from discord_webhook import DiscordEmbed, DiscordWebhook
from dotenv import load_dotenv
from markdownify import markdownify

logger = logging.getLogger(__name__)
handler = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)

load_dotenv()  # take environment variables from .env.

# vulnerabilities will be retrieved until now to "how_many_days_to_retrieve"
how_many_days_to_retrieve = 1

# Discord options for notifications
discord_webhook_url = os.getenv("DISCORD_HOOK_URL")


def log_info(msg):
    logger.info(msg)


def log_warning(msg):
    logger.warning(msg)


def log_error(msg):
    logger.error(msg)
    raise msg


def format_string(s):
    return s.replace("Ã©", "é")


class Severity(Enum):
    NONE = 1
    LOW = 2
    MEDIUM = 3
    HIGH = 4
    CRITICAL = 5


class CertFrDoc:

    NIST_CVE_DETAIL_PREFIX = "https://nvd.nist.gov/vuln/detail"

    def __init__(self, title: str, link: str) -> None:
        self.title = format_string(title)
        self.link = link
        self.is_cve: bool = False
        if "cve.mitre.org" in self.link:
            self.is_cve = True
        self.cve_id: str = None
        self.cve_nist_score: float = None
        self.cve_cna_score: float = None

    def __repr__(self) -> str:
        if self.is_cve:
            return f"{self.cve_id} - {self.link} - nist_score: {self.cve_nist_score} - cna_score: {self.cve_cna_score}"
        return f"{self.title} - {self.link}"

    def get_info(self) -> None:
        if self.is_cve:
            self.cve_id = self.link.split("=")[1]
            nist_link = f"{CertFrDoc.NIST_CVE_DETAIL_PREFIX}/{self.cve_id}"

            log_info(f"[CVE] {self.cve_id}")
            log_info(f"[CVE] Fetch CVE NIST data - {nist_link}")
            r = requests.get(nist_link)
            if r.status_code // 100 != 2 or "CVE ID Not Found" in r.text:
                log_warning(f"[CVE] Failed to fetch CVE NIST data - {nist_link}")
                return

            self.link = nist_link
            try:
                nist_score_str = r.text.split('id="Cvss3NistCalculatorAnchor"', 1)[1]
                nist_score_str = nist_score_str.split("</a>", 1)[0]
                nist_score_str = nist_score_str.split('">', 1)[1]
                self.cve_nist_score = float(nist_score_str.split(" ")[0])
            except Exception:
                log_warning(f"[CVE] Failed to get NIST Score for {self.cve_id}")

            try:
                cna_score_str = r.text.split('id="Cvss3CnaCalculatorAnchor"', 1)[1]
                cna_score_str = cna_score_str.split("</a>", 1)[0]
                cna_score_str = cna_score_str.split('">', 1)[1]
                self.cve_cna_score = float(cna_score_str.split(" ")[0])
            except Exception:
                log_warning(f"[CVE] Failed to get CNA Score for {self.cve_id}")

        else:
            log_info(f"[Doc] {self}")


class CertFrAlert:
    def __init__(
        self, title: str, link: str, pubdate: datetime, guid: str, description: str
    ) -> None:
        self.title = format_string(title)
        self.link = link
        self.pubdate = pubdate
        self.guid = guid
        self.description = str(markdownify(format_string(description))).strip()
        self.min_cvss_score: float = None
        self.max_cvss_score: float = None
        self.most_critical_vuln_nist_link: str = None
        self.severity: Severity = Severity.NONE
        self.docs: list[CertFrDoc] = []

    def __repr__(self) -> str:
        return f"{self.title} - {self.link}"

    def get_info(self) -> None:
        log_info(f"[Alert] Fetch CertFr alert data - {self}")
        r = requests.get(self.link)
        if r.status_code // 100 != 2:
            log_error(f"[Alert] Failed to fetch CertFr Alert - {self}")

        documentation = r.text.split("<h2>Documentation</h2>")
        documentation_ul = documentation[1].split("</ul>")[0].replace("<ul>", "")

        docs_li = documentation_ul.split("</li>")
        self.docs = []
        for li in docs_li:
            if not li.strip():
                continue
            li = li.strip().replace("\n", "").replace("<li>", "").replace("<br>", "")

            doc_title = li.split(" <a ")[0].strip()
            doc_link = li.split('">')[1].removesuffix("</a>").strip()
            self.docs.append(CertFrDoc(doc_title, doc_link))

        log_info("[Alert] Found " + str(len(self.docs)) + " docs for " + str(self))

        for d in self.docs:
            d.get_info()

            if d.is_cve:
                if d.cve_nist_score:
                    self.min_cvss_score = min(
                        d.cve_nist_score, self.min_cvss_score or 11.0
                    )
                    self.most_critical_vuln_nist_link = (
                        d.link
                        if d.cve_nist_score > (self.max_cvss_score or -1.0)
                        else None
                    )
                    self.max_cvss_score = max(
                        d.cve_nist_score, self.max_cvss_score or -1.0
                    )
                if d.cve_cna_score:
                    self.min_cvss_score = min(
                        d.cve_cna_score, self.min_cvss_score or 11.0
                    )
                    self.most_critical_vuln_nist_link = (
                        d.link
                        if d.cve_cna_score > (self.max_cvss_score or -1.0)
                        else None
                    )
                    self.max_cvss_score = max(
                        d.cve_cna_score, self.max_cvss_score or -1.0
                    )

        if self.max_cvss_score:
            if self.max_cvss_score >= 9:
                self.severity = Severity.CRITICAL
            elif self.max_cvss_score < 9 and self.max_cvss_score >= 7:
                self.severity = Severity.HIGH
            elif self.max_cvss_score < 7 and self.max_cvss_score >= 4:
                self.severity = Severity.MEDIUM
            elif self.max_cvss_score < 4 and self.max_cvss_score > 0:
                self.severity = Severity.LOW
            else:
                self.severity = Severity.NONE

            log_info(
                f"[Alert] {self} - {self.max_cvss_score} {self.severity} - {self.most_critical_vuln_nist_link}"
            )
        else:
            log_warning(f"[Alert] {self} - no score")


class CertFrRssFetcher:

    RSS_URL = "https://www.cert.ssi.gouv.fr/avis/feed/"
    START_DATE = datetime.now(timezone.utc) + timedelta(days=-how_many_days_to_retrieve)

    def __init__(self) -> None:
        self.alerts: list[CertFrAlert] = []

    def fetch(self):
        self.alerts = []

        log_info("[RSS] Fetch CERT-FR RSS feed")
        r = requests.get(CertFrRssFetcher.RSS_URL)
        if r.status_code // 100 != 2:
            log_error("[RSS] Failed to fetch CertFr RSS feed")

        root = ET.fromstring(r.text)
        for item in root.findall("channel/item"):
            item_title = item[0].text
            item_link = item[1].text
            item_pubdate = item[2].text
            item_guid = item[3].text
            item_description = item[4].text

            # pubdate format is : Mon, 24 Oct 2022 11:24:13 +0000
            pubdate = datetime.strptime(item_pubdate, "%a, %d %b %Y %H:%M:%S %z")
            # we don't take vuln older than last three days
            if pubdate < CertFrRssFetcher.START_DATE:
                continue

            self.alerts.append(
                CertFrAlert(item_title, item_link, pubdate, item_guid, item_description)
            )

        log_info("[RSS] Found " + str(len(self.alerts)) + " alerts")

        for alert in self.alerts:
            alert.get_info()
        self.alerts = sorted(self.alerts, key=lambda a: a.pubdate)

    def get_alerts(self) -> list[CertFrAlert]:
        return self.alerts


def discord_webhook_sender(alerts: list[CertFrAlert]):
    webhooks: list[DiscordWebhook] = []

    today_str = str(date.today())
    DISCORD_MESSAGE_HEADER = f"**Alerte du CERT-FR - {today_str}**"
    webhook = DiscordWebhook(
        url=discord_webhook_url, rate_limit_retry=True, content=DISCORD_MESSAGE_HEADER
    )
    webhooks.append(webhook)
    log_info(f"[Discord] {DISCORD_MESSAGE_HEADER}")

    for alert in alerts:
        color = None
        if alert.severity == Severity.CRITICAL:
            color = "ff2d00"
        elif alert.severity == Severity.HIGH:
            color = "ff8000"
        elif alert.severity == Severity.MEDIUM:
            color = "ffff00"
        elif alert.severity == Severity.LOW:
            color = "80ff00"
        else:
            color = "ffffff"  # None

        webhook = DiscordWebhook(
            url=discord_webhook_url, username="CERT-FR", rate_limit_retry=True
        )

        embed = DiscordEmbed(
            title=alert.title,
            description="**Alerte du CERT-FR** : "
            + alert.link
            + "\n**Score CVSS**: ``"
            + (str(alert.min_cvss_score) if alert.min_cvss_score else "N/A")
            + " -- "
            + (str(alert.max_cvss_score) if alert.max_cvss_score else "N/A")
            + "``"
            + "\n**Sévérité**: "
            + alert.severity.name
            + "\n**Vulnérabilité la plus critique**: "
            + (alert.most_critical_vuln_nist_link or "N/A")
            + "\n**Résumé**: "
            + alert.description
            + "\n",
            color=color,
        )
        embed.set_footer(
            text=alert.pubdate.strftime("%m/%d/%Y, %H:%M:%S") + " • CERT-FR"
        )

        for doc in alert.docs:
            if doc.is_cve:
                embed.add_embed_field(
                    name=doc.cve_id,
                    value="NIST score: "
                    + (str(doc.cve_nist_score) if doc.cve_nist_score else "N/A")
                    + " • CNA score: "
                    + (str(doc.cve_cna_score) if doc.cve_cna_score else "N/A")
                    + "\n"
                    + doc.link,
                    inline=False,
                )
            else:
                embed.add_embed_field(name=doc.title, value=doc.link)
        webhook.add_embed(embed)
        webhooks.append(webhook)
        log_info(f"[Discord] {alert}")

    log_info("[Discord] Sending webhooks")
    for w in webhooks:
        w.execute()
    log_info("[Discord] Done")


def main():
    start_time = time.time()

    fetcher = CertFrRssFetcher()
    fetcher.fetch()

    alerts = fetcher.get_alerts()

    discord_webhook_sender(alerts)

    duration = time.time() - start_time
    log_info(f"[Metrics] duration: {duration}s")


if __name__ == "__main__":
    main()
